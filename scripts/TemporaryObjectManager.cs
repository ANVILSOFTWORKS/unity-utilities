using UnityEngine;
using System.Linq;
using System.Collections.Generic;
namespace Anvil.Loot.Scifi.Utilities
{
    public class TemporaryObjectManager : GameObjectBehaviour
    {

        public List<Pool> m_Pools = new List<Pool>();
        public int m_MinimumFree = 8;
        public float m_DefaultTimeToLive = 3;

        public static TemporaryObjectManager m_Instance;

        void Start()
        {
            m_Instance = this;
        }

        protected override void FixedUpdate10()
        {
            int tot = 0;

            for (int i = 0; i < m_Pools.Count; i++)
            {
                tot += m_Pools[i].Update(Constants.SLOW_UPDATE_10_LIMIT);
            }
            Utilities.Log("Pools: " + m_Pools.Count + "\t\tObject Total: " + tot);
        }

        int GetID(GameObject go)
        {
            for (int i = 0; i < m_Pools.Count; i++)
            {
                if (go.GetInstanceID() == m_Pools[i].m_Prefab.GetInstanceID())
                {
                    return i;
                }
            }
            return -1;
        }

        public static List<GameObject> GetNearby(GameObject go, Vector3 pos, float radius)
        {
            var retVal = new List<GameObject>();

            int id = m_Instance.GetID(go);

            if (id >= 0)
            {
                foreach (GameObject g in m_Instance.m_Pools[id].m_Elements)
                {
                    if (g.activeSelf && Vector3.Distance(g.transform.position, pos) <= radius)
                    {
                        retVal.Add(g);
                    }
                }
            }
            return retVal;
        }
        public static GameObject Spawn(GameObject go)
        {
            return Spawn(go, m_Instance.m_DefaultTimeToLive, Vector3.zero, Quaternion.identity);
        }
        public static GameObject Spawn(GameObject go, Vector3 position)
        {
            return Spawn(go, m_Instance.m_DefaultTimeToLive, position, Quaternion.identity);
        }
        public static GameObject Spawn(GameObject go, Vector3 position, Vector3 forward)
        {
            GameObject g = Spawn(go, m_Instance.m_DefaultTimeToLive, position, Quaternion.identity);
            g.transform.forward = forward;
            return g;
        }
        public static GameObject Spawn(GameObject go, Vector3 position, Quaternion rotation)
        {
            return Spawn(go, m_Instance.m_DefaultTimeToLive, position, rotation);
        }
        public static GameObject Spawn(GameObject go, float time)
        {
            return Spawn(go, time, Vector3.zero, Quaternion.identity);
        }
        public static GameObject Spawn(GameObject go, float time, Vector3 position)
        {
            return Spawn(go, time, position, Quaternion.identity);
        }
        public static GameObject Spawn(GameObject go, float time, Vector3 position, Vector3 forward)
        {
            GameObject g = Spawn(go, time, position, Quaternion.identity);
            g.transform.forward = forward;
            return g;
        }
        public static GameObject Spawn(GameObject go, float time, Vector3 position, Quaternion rotation)
        {
            int id = m_Instance.GetID(go);

            //if it doesn't exist, we have to make one
            if (id == -1)
            {
                Pool p = new Pool(go);
                m_Instance.m_Pools.Add(p);
                Utilities.Log("Made the Pool");
                return p.Spawn(time, position, rotation);
            }
            else
            {
                return m_Instance.m_Pools[id].Spawn(time, position, rotation);
            }
        }
    }

    public class Pool : UnityEngine.Object
    {
        public List<GameObject> m_Elements = new List<GameObject>();
        public List<float> m_LifeSpans = new List<float>();
        public GameObject m_Prefab;

        int m_InUse = 0;

        public Pool(GameObject go)
        {
            m_Prefab = go;
            Utilities.Log("About to Make Space");
            MakeMoreSpace();
        }

        void MakeMoreSpace()
        {

            for (int i = 0; i < 1 + TemporaryObjectManager.m_Instance.m_MinimumFree; i++)
            {
                Utilities.Log("MMS-Index-" + i);
                GameObject go2 = Object.Instantiate(m_Prefab);
                go2.SetActive(false);

                if (!(go2.transform is RectTransform))
                {
                    go2.transform.parent = TemporaryObjectManager.m_Instance.transform;
                }
                m_Elements.Add(go2);
                m_LifeSpans.Add(-10);
            }
        }

        public int Update(float delta)
        {
            for (int i = 0; i < m_Elements.Count; i++)
            {
                if (m_LifeSpans[i] > 0)
                {
                    m_LifeSpans[i] -= delta;
                }
                else if (m_LifeSpans[i] > -9)
                {
                    m_LifeSpans[i] = -10;
                    m_Elements[i].SetActive(false);

                    m_InUse--;
                }
            }
            return m_Elements.Count;
        }

        public GameObject Spawn(float time, Vector3 position, Quaternion rotation)
        {
            int index = m_LifeSpans.FindIndex(a => a == -10);

            Utilities.Log("S-Index-" + index);

            if (index != -1)
            {
                if (!m_Elements[index].activeSelf)
                {
                    Utilities.Log("S-Active==false");

                    m_Elements[index].transform.position = position;
                    m_Elements[index].transform.rotation = rotation;
                    m_LifeSpans[index] = time;
                    m_Elements[index].SetActive(true);
                    m_Elements[index].SendMessage("Awake", SendMessageOptions.DontRequireReceiver);

                    m_InUse++;

                    return m_Elements[index];
                }
                else
                {
                    Debug.LogError("Object Pooler: Lifespans do not reflect state of pooled objects!");
                    Debug.Break();
                    return null;
                }
            }
            else
            {
                MakeMoreSpace();
                return Spawn(time, position, rotation);
            }
        }
    }
}