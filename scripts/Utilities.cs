using UnityEngine;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Linq;
using System;

namespace Anvil.Loot.Scifi.Utilities
{
    public static class Utilities
    {
        static bool Logging = false;
        public static string SubStringSafe(this string s, int i, int len)
        {
            if (s.Length > i + len)
            {
                if (s[i + len - 1] == '.')
                {
                    return s.Substring(i, len - 1);
                }
                return s.Substring(i, len);
            }
            return s;
        }
        public static string Display(this int f)
        {
            return ((double)f).Display(false);
        }
        public static string Display(this float f)
        {
            return ((double)f).Display(false);
        }
        public static string Display(this double f)
        {
            return ((double)f).Display(false);
        }
        public static string Display(this int f, bool decimals)
        {
            return ((double)f).Display(decimals);
        }
        public static string Display(this float f, bool decimals)
        {
            return ((double)f).Display(decimals);
        }
        public static string Display(this double f, bool decimals)
        {
            bool negative = f < 0;
            f = System.Math.Abs(f);
            if (f < 0.1f)
            {
                return "0";
            }
            else if (f < 10f && !decimals)
            {
                return ((int)f).ToString();
            }
            double temp = f;
            int index = 0;
            string[] Orders = { "", "K", "M", "G", "T", "P", "E", "Z", "Y" };
            while (temp >= 999.999)
            {
                temp /= 1000.0;
                index++;
            }
            if (negative)
            {
                temp = -temp;
            }
            if (index >= Orders.Length)
            {
                return temp.ToString().SubStringSafe(0, 3) + (char)(index - Orders.Length + 945);
            }
            return temp.ToString().SubStringSafe(0, 3) + Orders[index];
        }
        public static void Shuffle<T>(this IList<T> list)
        {
            System.Random rng = new System.Random();
            int n = list.Count / 2;
            while (n > 1)
            {
                n--;
                int k = rng.Next(2 * n + 1);
                T value = list[k];
                list[k] = list[2 * n];
                list[2 * n] = value;
            }
        }
        public static List<T> Append<T>(this List<T> list, T t)
        {
            list.Add(t);
            return list;
        }
        public static List<T> ToList<T>(this T[] arr)
        {
            List<T> newList = new List<T>();
            foreach (T item in arr)
            {
                newList.Add(item);
            }
            return newList;
        }
        public static void Log(string s)
        {
            if (!Logging) return;
            StreamWriter UtilitiesLog = new StreamWriter("Utilities.Log", true);
            string Name = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            Name = Name.Substring(Name.LastIndexOf("\\") + 1);
            UtilitiesLog.WriteLine("User: " + Name +
                              "\t\tLevel: " + Application.loadedLevelName +
                              "\t\tTime: " + (int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1))).TotalSeconds +
                              "\t\t" + s);
            UtilitiesLog.Close();
        }

        private static Rect Rect = new Rect();
        private static float RectSum = 0f;

#if UNITY_EDITOR
        public static Rect RectStart(float x, float y, float width)
        {
            RectSum = width;
            Rect = new Rect(x, y, width, EditorGUIUtility.singleLineHeight);
            return Rect;
        }
#endif
        public static Rect RectWidth(float width)
        {
            RectSum += width;
            Rect = new Rect(Rect.x + Rect.width, Rect.y, width, Rect.height);
            return Rect;
        }
        public static Rect RectMax(float right)
        {
            float width = right - RectSum;
            RectSum += width;
            Rect = new Rect(Rect.x + Rect.width, Rect.y, width, Rect.height);
            return Rect;
        }
        // Rect transform extenstions from http://stackoverflow.com/questions/26423549/how-to-modify-recttransform-properties-in-script-unity-4-6-beta
        public static void SetDefaultScale(this RectTransform trans)
        {
            trans.localScale = new Vector3(1, 1, 1);
        }
        public static void SetPivotAndAnchors(this RectTransform trans, Vector2 aVec)
        {
            trans.pivot = aVec;
            trans.anchorMin = aVec;
            trans.anchorMax = aVec;
        }
        public static Vector2 GetSize(this RectTransform trans)
        {
            return trans.rect.size;
        }
        public static float GetWidth(this RectTransform trans)
        {
            return trans.rect.width;
        }
        public static float GetHeight(this RectTransform trans)
        {
            return trans.rect.height;
        }
        public static void SetPositionOfPivot(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x, newPos.y, trans.localPosition.z);
        }
        public static void SetLeftBottomPosition(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
        }
        public static void SetLeftTopPosition(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
        }
        public static void SetRightBottomPosition(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
        }
        public static void SetRightTopPosition(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
        }
        public static void SetSize(this RectTransform trans, Vector2 newSize)
        {
            Vector2 oldSize = trans.rect.size;
            Vector2 deltaSize = newSize - oldSize;
            trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
            trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
        }
        public static void SetWidth(this RectTransform trans, float newSize)
        {
            SetSize(trans, new Vector2(newSize, trans.rect.size.y));
        }
        public static void SetHeight(this RectTransform trans, float newSize)
        {
            SetSize(trans, new Vector2(trans.rect.size.x, newSize));
        }
#if UNITY_EDITOR
        public static void EmbedEditor<T>(this Editor e, T o)
            where T : UnityEngine.Object
        {
            if (o != null)
            {
                SerializedObject Sero = new SerializedObject(o);
                if (Sero != null)
                {
                    SerializedProperty prop = Sero.GetIterator();
                    while (prop.NextVisible(true))
                    {
                        EditorGUILayout.PropertyField(prop);
                    }
                    prop.Reset();
                }
                Sero.ApplyModifiedProperties();
            }
        }
#endif

        public static T RandomItem<T>(this IEnumerable<T> data)
        {
            var l = data.ToList();

            return l[UnityEngine.Random.Range(0, l.Count())];
        }
        public static List<T> Copy<T>(this List<T> listToClone)
        {
            return listToClone.ToList();
        }
        public static IEnumerable<IEnumerable<T>> CartesianProduct<T>(this IEnumerable<IEnumerable<T>> sequences)
        {
            IEnumerable<IEnumerable<T>> emptyProduct = new[] { Enumerable.Empty<T>() };
            return sequences.Aggregate(
                emptyProduct,
                (accumulator, sequence) =>
                    from accseq in accumulator
                    from item in sequence
                    select accseq.Concat(new[] { item })
                );
        }
        public static IEnumerable<List<T>> CreateCombinations<T>(this List<T> items, int length) where T : class
        {
            if (length == 1)
                return items.Select(item => new List<T>() { item });

            return from a in items
                   from b in CreateCombinations(items, length - 1)
                   where !b.Any(x => x == a)
                   select b.Extend(a);
        }

        public static List<T> Extend<T>(this List<T> items, params T[] item) where T : class
        {
            items.AddRange(item);

            return items;
        }

        public static Texture2D textureFromSprite(this Sprite sprite)
        {
            if (sprite.rect.width != sprite.texture.width)
            {
                Texture2D newText = new Texture2D((int)sprite.texture.width, (int)sprite.texture.height);

                Color32[] newColors = sprite.texture.GetPixels32();

                newText.SetPixels32(newColors);
                newText.Apply();
                return newText;
            }
            return sprite.texture;
        }


        public static Texture2D AlphaBlend(this Texture2D bottom, Texture2D top, int dx = 0, int dy = 0)
        {
            int rdx = dx, rdy = dy;

            //TODO: make dx and dy the center-based offset



            int width, height, bx = 0, by = 0;


            height = Math.Max(top.height + rdy, bottom.height);
            width = Math.Max(top.width + rdx, bottom.width);


            //todo: calculate values

            var res = new Texture2D(width, height);


            res.SetPixels32(bx, by, bottom.width, bottom.height, bottom.GetPixels32());
            res.SetPixels32(rdx, rdy, top.width, top.height, top.GetPixels32());

            res.Apply();

            return res;
        }

        public delegate void FunctionEventTrigger();
    }

}