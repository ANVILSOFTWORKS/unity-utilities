﻿using UnityEngine;
using System.Collections;

namespace Anvil.Loot.Scifi.Utilities
{
    public class GameObjectBehaviourTimer : MonoBehaviour
    {

        private float m_Thirty = 0f;
        private float m_Tenth = 0f;
        private float m_Fifth = 0f;
        private float m_Half = 0f;
        private float m_Second = 0f;

        public bool[,] m_UpdateStatus = new bool[5, Constants.SLOW_UPDATE_OFFSET_COUNT];

        //let's make and manage our instance
        private static GameObjectBehaviourTimer m_Instance = null;

        public static GameObjectBehaviourTimer GetInstance()
        {
            //If _instance hasn't been set yet, we grab it from the scene!
            //This will only happen the first time this reference is used.
            if (m_Instance == null)
                m_Instance = GameObject.FindObjectOfType<GameObjectBehaviourTimer>();
            return m_Instance;
        }

        //early intialization
        void Awake()
        {
            //initialization to get around the editor's bull
            m_UpdateStatus = new bool[5, Constants.SLOW_UPDATE_OFFSET_COUNT];

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < Constants.SLOW_UPDATE_OFFSET_COUNT; j++)
                {
                    m_UpdateStatus[i, j] = false;
                }
            }
        }

        void FixedUpdate()
        {
            float t = Time.deltaTime;

            m_Thirty += t;
            m_Tenth += t;
            m_Fifth += t;
            m_Half += t;
            m_Second += t;

            for (int i = 0; i < 5; i++)
            {
                for (int j = Constants.SLOW_UPDATE_OFFSET_COUNT - 1; j > 0; j--)
                {
                    m_UpdateStatus[i, j] = m_UpdateStatus[i, j - 1];
                }
            }

            m_UpdateStatus[0, 0] = m_Thirty > Constants.SLOW_UPDATE_30_LIMIT;
            m_UpdateStatus[1, 0] = m_Tenth > Constants.SLOW_UPDATE_10_LIMIT;
            m_UpdateStatus[2, 0] = m_Fifth > Constants.SLOW_UPDATE_5_LIMIT;
            m_UpdateStatus[3, 0] = m_Half > Constants.SLOW_UPDATE_2_LIMIT;
            m_UpdateStatus[4, 0] = m_Second > Constants.SLOW_UPDATE_1_LIMIT;

            if (m_UpdateStatus[0, 0])
            {
                m_Thirty = 0f;
            }
            if (m_UpdateStatus[1, 0])
            {
                m_Tenth = 0f;
            }
            if (m_UpdateStatus[2, 0])
            {
                m_Fifth = 0f;
            }
            if (m_UpdateStatus[3, 0])
            {
                m_Half = 0f;
            }
            if (m_UpdateStatus[4, 0])
            {
                m_Second = 0f;
            }
        }
    }
}