using UnityEngine;
using System.Collections;
using System;
namespace Anvil.Loot.Scifi.Utilities
{
    public class Timer
    {
        public float Counter = 0f;
        float initTime = 0f;

        public Timer(float counter)
        {
            Counter = counter;
            initTime = Time.realtimeSinceStartup - Counter;
        }

        public bool Value()
        {
            return Time.realtimeSinceStartup >= Counter + initTime;
        }

        public float Till()
        {
            return Counter + initTime - Time.realtimeSinceStartup;
        }

        public void Reset()
        {
            initTime = Time.realtimeSinceStartup;
        }

        public bool UseIfPossible()
        {
            if (Value())
            {
                initTime = Time.realtimeSinceStartup;
                return true;
            }
            return false;
        }

        public static long TimeSinceEpoch()
        {
            var unixTime = System.DateTime.Now.ToUniversalTime() -
                new System.DateTime(2015, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

            return (long)unixTime.TotalMilliseconds;
        }

        internal void Reset(float counter)
        {
            Reset();
            Counter = counter;
        }
    }
}