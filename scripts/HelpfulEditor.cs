using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using UnityEngine;

using Anvil.Loot.Scifi.Utilities;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif



#if UNITY_EDITOR
namespace Anvil.Loot.Scifi.Utilities
{
    public class HelpfulEditor : Editor
    {
        protected bool _helpFoldStatus = false;
        protected static GUIStyle FoldStyle = null;
        protected static GUIStyle LabelStyle = null;

        protected virtual string HelpText()
        {
            return "";
        }
        public override void OnInspectorGUI()
        {
            if (LabelStyle == null)
            {
                LabelStyle = new GUIStyle(EditorStyles.label);
                LabelStyle.normal.textColor = new Color(0.4f, 0.4f, 0.4f);
                LabelStyle.wordWrap = true;
            }
            if (FoldStyle == null)
            {
                FoldStyle = new GUIStyle(EditorStyles.foldout);
                FoldStyle.normal.textColor = new Color(0, 0.2f, 0.8f);
                //FoldStyle.fontStyle = FontStyle.Bold;
                FoldStyle.margin = new RectOffset(0, 0, 5, 10);
                FoldStyle.fontSize = 16;
            }


            EditorGUIUtility.labelWidth = 100;

            base.OnInspectorGUI();


            _helpFoldStatus = EditorGUILayout.Foldout(_helpFoldStatus, "Explanation", FoldStyle);
            if (_helpFoldStatus)
            {
                GUILayout.Label(HelpText(), LabelStyle);
            }


        }

    }
}
#endif
