﻿using UnityEngine;
using System.Collections;

namespace Anvil.Loot.Scifi.Utilities
{
    public static partial class Constants
    {
        //Game Object Behaviour constants
        public const float SLOW_UPDATE_30_LIMIT = 1f / 30f;
        public const float SLOW_UPDATE_10_LIMIT = 1f / 10f;
        public const float SLOW_UPDATE_5_LIMIT = 1f / 5f;
        public const float SLOW_UPDATE_2_LIMIT = 1f / 2f;
        public const float SLOW_UPDATE_1_LIMIT = 1f;
        public const int SLOW_UPDATE_OFFSET_COUNT = 6;
    }
}